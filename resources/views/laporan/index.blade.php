@extends('layouts.app')

@section('title')
	Laporan Pendapatan {{ tanggal_indonesia($awal) }} s/d {{ tanggal_indonesia($akhir) }}
@endsection

@section('breadcrumb')
	@parent
	<li>Laporan</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-header with-border">
			        <a onclick="periodeForm()" class="btn btn-success btn-xs btn-flat"><i class="fa fa-plus-circle"></i> Ubah Periode</a>
			        <a href="laporan/pdf/{{ $awal }}/{{ $akhir }}" target="_blank" class="btn btn-xs btn-info btn-flat"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
			    </div>
			
			    <div class="box-body" style="overflow: auto;">
			        <table class="table table-striped table-bordered table-laporan">
			            <thead>
			                <th width="30">No</th>
			                <th>Tanggal</th>
			                <th>Penjualan</th>
			                <th>Pembelian</th>
			                <th>Pengeluaran</th>
			                <th>Pendapatan</th>
			            </thead>
			            <tbody></tbody>
			        </table>
			    </div>
			</div>
		</div>
	</div>
	@include('laporan.form')
@endsection

@section('script')
	<script>
		var table, awal, akhir;
		
		$(function() {
			$('#awal, #akhir').datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true
			})

			table = $('.table-laporan').DataTable({
				'dom': 'Brt',
				'bSort': false,
				'bPaginate': false,
				'processing' : true,
				'autoWidth': false,
				'ajax' : {
					'url' : 'laporan/data/{{ $awal }}/{{ $akhir }}',
					'type' : 'GET'
				}
			});
		})

		function periodeForm(){
			$('#modal-form').modal('show')
		}
	</script>
@endsection
