<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak Kartu Member</title>
	<style>
		.box{position: relative;}
		.card{width: 501.732pt; height: 147.402pt;}
		.kode{
			position: absolute;
			top: 100pt;
			left: 110pt;
			color: #fff;
			font-size: 10pt;
		}
		.nama{
			position: absolute;
			top: 65pt;
			left: 115pt;
			font-size: 15pt;
		}
		.barcode{
			position: absolute;
			top: 5pt;
			left: 280pt;
			font-size: 10pt;
		}
	</style>
</head>
<body>
	<table width="100%">
		@foreach($datamember as $data)
			<tr>
				<td align="center">
					<div class="box">
						<img src="{{ asset('public/images/') }}/{{ $card->kartu_member }}" class="card">
						<div class="kode">{{ $data->kode_member }}</div>
						<div class="nama">{{ $data->nama }}</div>
						<div class="barcode">
							<img src="data:image/png;base64,{{ DNS1D::getBarcodePNG($data->kode_member, 'C39') }}" height="33" width="220">
							<br>{{ $data->kode_member }}
						</div>
					</div>
				</td>
			</tr>
		@endforeach
	</table>
</body>
</html>