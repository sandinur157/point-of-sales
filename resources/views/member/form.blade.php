<div class="modal fade" id="modal-form" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="fom-horizontal" method="post" data-toggle="validator">
				@csrf {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
					<h4 class="modal-title"></h4>
				</div>

				<div class="modal-body">
					<input type="hidden" id="id" name="id">

					<div class="form-group row">
						<label for="kode" class="col-md-2 col-md-offset-1 control-label">Kode Member</label>
						<div class="col-md-6">
							<input type="text" name="kode" id="kode" class="form-control" required readonly>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="nama" class="col-md-2 col-md-offset-1 control-label">Nama Member</label>
						<div class="col-md-6">
							<input type="text" name="nama" id="nama" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="alamat" class="col-md-2 col-md-offset-1 control-label">Alamat</label>
						<div class="col-md-6">
							<input type="text" name="alamat" id="alamat" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="telpon" class="col-md-2 col-md-offset-1 control-label">Telpon</label>
						<div class="col-md-6">
							<input type="text" name="telpon" id="telpon" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-flat btn-primary btn-save"><i class="fa fa-floppy-o"></i>  Simpan</button>
					<button type="button" class="btn btn-sm btn-flat btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>