@extends('layouts.app')

@section('title')
	Edit Profil
@endsection

@section('breadcrumb')
	@parent
	<li>Edit Profil</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<form class="form fom-horizontal" method="post" data-toggle="validator" enctype="multipart/form-data">
				@csrf {{ method_field('PATCH') }}
				<div class="box-body" style="padding-top: 1em;">
					<div class="alert alert-info" role="alert" style="display: none;">
					    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    Perubahan berhasil disimpan
					</div>

					<div class="form-group row">
					    <label for="nama" class="col-sm-2 control-label">Nama</label>
					    <div class="col-sm-6">
					    	<input type="text" name="nama" id="nama" class="form-control" required>
					    </div>
					</div>
					
					<div class="form-group row">
					    <label for="foto" class="col-sm-2 control-label">Foto</label>
					    <div class="col-sm-4">
					    	<input type="file" name="foto" id="foto" class="form-control">
					    	<br>
					    	<div class="tampil-foto">
					    		<img src="{{ asset('public/images/'. Auth::user()->foto) }}" width="200"style="border: 1px solid #ccc; box-shadow: 0 0 1px rgba(0,0,0,.25);">
					    	</div>
					    </div>
					</div>
					
					<div class="form-group row">
					    <label for="passwordlama" class="col-sm-2 control-label">Password Lama</label>
					    <div class="col-sm-6">
					    	<input type="password" name="passwordlama" id="passwordlama" class="form-control">
					    </div>
					</div>
					
					<div class="form-group row">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-6">
							<input type="password" name="password" id="password" class="form-control">
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="password1" class="col-md-2 control-label">Ulang Password</label>
						<div class="col-md-6">
							<input type="password" name="password1" id="password1" class="form-control" data-match="#password">
							<span class="help-block with-errors"></span>
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button class="btn btn-sm btn-flat btn-primary pull-right"><i class="fa fa-floppy-o"></i>  Simpan Perubahan</button>
				</div>
			</form>			    
			</div>
		</div>
	</div>
@endsection

@section('script')
<script>
	$(function() {
		// 1. saat password lama diubah
		$('#passwordlama').keyup(function() {
			if($(this).val() != '') $('#password, #password1').attr('required', true);
			else $('#password, #password1').attr('required', false);
		})

		$('.form').validator().on('submit', function(e) {
			if(!e.isDefaultPrevented()) {
				// upload file via ajax
				$.ajax({
					url : '{{ route('profil.update', Auth::user()->id) }}',
					type : 'POST',
					data : new FormData($('.form') [0]),
					dataType : 'JSON',
					async : false,
					processData : false,
					contentType : false,
					success : function(data) {
						// tampilakn pesan jika msg=error
						if(data.msg == 'error') {
							alert('Password lama salah!');
							$('#passwordlama').focus().select();
						} else {
							d = new Date();
							$('.alert').css('display', 'block').delay(2000).fadeOut();

							// update foto user
							$('.tampil-foto img, #user-sidebar, .user-image, .user-header img').attr('src', data.url+'?'+d.getTime());
							$('#name-navbar, #name-dropdown-menu, #name-sidebar').text(data.name);
						}

						// reset
						$('#passwordlama, #password, #password1, #foto, #nama').val('')
					},
					error : function() {
						alert('Tidak dapat menyimpan data!');
					}
				});
				return false;
			}
		})
	})
</script>
@endsection
