<div class="modal fade" id="modal-form" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="fom-horizontal" method="post" data-toggle="validator">
				@csrf {{ method_field('POST') }}
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
					<h4 class="modal-title"></h4>
				</div>

				<div class="modal-body">
					<input type="hidden" id="id" name="id">

					<div class="form-group row">
						<label for="nama" class="col-md-2 col-md-offset-1 control-label">Nama User</label>
						<div class="col-md-6">
							<input type="text" name="nama" id="nama" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-md-2 col-md-offset-1 control-label">Email</label>
						<div class="col-md-6">
							<input type="text" name="email" id="email" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-md-2 col-md-offset-1 control-label">Password</label>
						<div class="col-md-6">
							<input type="text" name="password" id="password" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>

					<div class="form-group row">
						<label for="password1" class="col-md-2 col-md-offset-1 control-label">Ulang Password</label>
						<div class="col-md-6">
							<input type="text" name="password1" id="password1" class="form-control" required data-match="#password">
							<span class="help-block with-errors"></span>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-sm btn-flat btn-primary btn-save"><i class="fa fa-floppy-o"></i>  Simpan</button>
					<button type="button" class="btn btn-sm btn-flat btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>