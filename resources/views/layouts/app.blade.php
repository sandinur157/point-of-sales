<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $setting->nama_perusahaan }} | @yield('title')</title>
    <link rel="icon" href="{{ asset('public/images/') }}/{{ $setting->logo }}" type="image/gif">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/dataTables/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('public/AdminLTE/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .box {border-top: 3px solid #605ca8;}
    </style>
    @yield('css')
</head>

<body class="hold-transition skin-purple-light sidebar-mini">

    <div class="wrapper">
        <!-- Header -->
        <header class="main-header" id="main-header">

            <a href="{{ url('/') }}" class="logo">
                <span class="logo-mini">{{ $setting->nama_perusahaan }}</span>
                <span class="logo-lg"><b>{{ $setting->nama_perusahaan }}</b></span>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <img src="{{ asset('public/images/') }}/{{ Auth::user()->foto }}" class="user-image" alt="User Image">
                          <span class="hidden-xs" id="name-navbar">{{ Auth::user()->name }}</span>
                        </a>

                            <ul class="dropdown-menu">
                                <li class="user-header" style="height: auto;">
                                    <img src="{{ asset('public/images/') }}/{{ Auth::user()->foto }}" class="img-circle" alt="User Image">
                                    <p id="name-dropdown-menu">{{ Auth::user()->name }}</p>
                                </li>

                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ url('user/profil') }}" class="btn btn-default btn-flat">Edit Profil</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- End Header -->

        <!-- Sidebar -->
        <aside class="main-sidebar">
            <section class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{ asset('public/images/') }}/{{ Auth::user()->foto }}" class="img-circle user-image" alt="User Image" id="user-sidebar">
                    </div>
                    <div class="pull-left info">
                        <p id="name-sidebar">{{ Auth::user()->name }}</p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>

                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>
                    {{-- <li class="header" style="background-color: #222d32; border-top: 1px solid #454545;">MAIN NAVIGATION</li> --}}
                    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> <span> Dashboard</span></a></li>

                    @if(Auth::user()->level == 1)
                    <li><a href="{{ url('/kategori') }}"><i class="fa fa-cube"></i> <span>Kategori</span></a></li>
                    <li><a href="{{ url('/produk') }}"><i class="fa fa-cubes"></i> <span>Produk</span></a></li>
                    <li><a href="{{ url('/member') }}"><i class="fa fa-credit-card"></i> <span>Member</span></a></li>
                    <li><a href="{{ url('/supplier') }}"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>
                    <li><a href="{{ url('/pengeluaran') }}"><i class="fa fa-money"></i> <span>Pengeluaran</span></a></li>
                    <li><a href="{{ url('/user') }}"><i class="fa fa-user"></i> <span>User</span></a></li>
                    <li><a href="{{ url('/pembelian') }}"><i class="fa fa-download"></i> <span>Pembelian</span></a></li>
                    <li><a href="{{ url('/penjualan') }}"><i class="fa fa-upload"></i> <span>Penjualan</span></a></li>
                    <li><a href="{{ url('/transaksi') }}"><i class="fa fa-shopping-cart"></i> <span>Transaksi Lama</span></a></li>
                    <li><a href="{{ url('/transaksi/baru') }}"><i class="fa fa-cart-plus"></i> <span>Transaksi Baru</span></a></li>
                    <li><a href="{{ url('/laporan') }}"><i class="fa fa-file-pdf-o"></i> <span>Laporan</span></a></li>
                    <li><a href="{{ url('/setting') }}"><i class="fa fa-gears"></i> <span>Setting</span></a></li>

                    @elseif(Auth::user()->level == 2)
                    <li><a href="{{ url('/transaksi') }}"><i class="fa fa-shopping-cart"></i> <span>Transaksi Lama</span></a></li>
                    <li><a href="{{ url('/transaksi/baru') }}"><i class="fa fa-cart-plus"></i> <span>Transaksi Baru</span></a></li>

                    @endif
                </ul>
            </section>
        </aside>
        <!-- End Sidebar -->

        <!-- Content -->
        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                    @yield('title')
                </h1>
                <ol class="breadcrumb">
                    @section('breadcrumb')
                    <li><a href="/"><i class="fa fa-home"></i> Home</a></li>
                    @show
                </ol>
            </section>

            <section class="content container-fluid">
                @yield('content')
            </section>
        </div>
        <!-- End Content -->

        <!-- Footer -->
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                Aplikasi POS oleh: Nur Sandi
            </div>

            <strong>Copyright &copy; 2018.</strong> All rights reserved.
        </footer>
        <!-- End Footer -->
    </div>

    <script src="{{ asset('public/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/chartjs/Chart.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/dataTables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/dataTables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/validator/validator.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ asset('public/AdminLTE/dist/js/AdminLTE.min.js') }}"></script>
    <script>

        $('.sidebar-menu a').filter(function() {
            return this.href == window.location.href;
        }).parent().addClass('active');
    </script>
    @yield('script')

</body>

</html>