@extends('layouts.app')

@section('title')
	Export Excel
@endsection

@section('breadcrumb')
	@parent
	<li><a href="/produk">Produk</a></li>
	<li>Export/Import Excel</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<form action="/importproduk" method="post" enctype="multipart/form-data"> 
				@csrf
			    <div class="box-header with-border">
			    	<h4 class="box-title">Export/Import Excel</h4>
			    </div>
			    <div class="box-body">
			    	<div class="form-group row">
			    		<div class="col-md-3">
			    			<a href="/exportproduk" class="btn btn-success">Export</a>
			    			<br><br>
			    		</div>
			    	</div>

			    	<div class="form-group row">
			    		<label for="file" class="control-label col-md-2">File Excel</label>
		    			<div class="col-md-3">
		    				<input type="file" class="form-control" id="file" name="file">
		    			</div>
			    	</div>
			    </div>
			    <div class="box-footer">
			    	<button class="btn btn-primary">Import</button>
			    </div>
			    </form>
			</div>
		</div>
	</div>
@endsection
