@extends('layouts.app')

@section('title')
	Dashboard
@endsection

@section('bredcrumb')
	@parent
	<li>Dashboard</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
			    <div class="box-body text-center">
			    	<h1>Selamat Datang</h1>
			    	<h2>Anda login sebagai KASIR</h2>
			    	<br><br>
			    	<a href="{{ route('transaksi.new') }}" class="btn btn-success btn-lg">Transaksi Baru</a>
			    	<br><br><br>
			    </div>
			</div>
		</div>
	</div>
@endsection