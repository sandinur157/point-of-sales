@extends('layouts.app')

@section('title')
	Pengaturan
@endsection

@section('breadcrumb')
	@parent
	<li>Pengaturan</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box" style="padding-top: 1em;">
				<form class="form fom-horizontal" method="post" data-toggle="validator" enctype="multipart/form-data">
				@csrf {{ method_field('PATCH') }}
			    <div class="box-body">
			    	<div class="alert alert-info alert-dismissible" style="display: none;">
			    		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    		<i class="icon fa fa-check"></i> Perubahan berhasil disimpan
			    	</div>

			        <div class="form-group row">
						<label for="nama" class="col-md-2 col-md-offset-1 control-label">Nama Perusahaan</label>
						<div class="col-md-6">
							<input type="text" name="nama" id="nama" class="form-control" required autofocus>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group row">
						<label for="alamat" class="col-md-2 col-md-offset-1 control-label">Alamat</label>
						<div class="col-md-8">
							<textarea name="alamat" id="alamat" rows="3" class="form-control" required></textarea>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group row">
						<label for="telepon" class="col-md-2 col-md-offset-1 control-label">Telepon</label>
						<div class="col-md-4">
							<input type="text" name="telepon" id="telepon" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group row">
						<label for="logo" class="col-md-2 col-md-offset-1 control-label">Logo Perusahaan</label>
						<div class="col-md-4">
							<input type="file" name="logo" id="logo" class="form-control">
							<br><div class="tampil-logo"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="kartu_member" class="col-md-2 col-md-offset-1 control-label">Desain Kartu Member</label>
						<div class="col-md-4">
							<input type="file" name="kartu_member" id="kartu_member" class="form-control">
							<br><div class="tampil-kartu"></div>
						</div>
					</div>
					<div class="form-group row">
						<label for="diskon_member" class="col-md-2 col-md-offset-1 control-label">Diskon Member (%)</label>
						<div class="col-md-2">
							<input type="number" name="diskon_member" id="diskon_member" class="form-control" required>
							<span class="help-block with-errors"></span>
						</div>
					</div>
					<div class="form-group row">
						<label for="tipe_nota" class="col-md-2 col-md-offset-1 control-label">Tipe Nota</label>
						<div class="col-md-2">
							<select name="tipe_nota" id="tipe_nota" class="form-control">
								<option value="0">Nota Kecil</option>
								<option value="1">Nota Besar (PDF)</option>
							</select>
						</div>
					</div>
			    </div>
			    <div class="box-footer">
			    	<button type="submit" class="btn btn-primary btn-sm btn-flat pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>
			    </div>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script>
		$(function() {
			showData()
			$('.form').validator().on('submit', function(e) {
				if(!e.isDefaultPrevented()) {
					$.ajax({
						url: 'setting/'+ 1,
						type: 'POST',
						data: new FormData($('.form') [0]),
						async: false,
						processData: false,
						contentType: false,
						success: function(data) {
							showData()
							$('.alert').css('display', 'block').delay(2000).fadeOut();
						},
						error: function(data) {
							alert('Tidak dapat menyimpan data!')
						}
					})
					return false;
				}
			}) 
		})

		function showData() {
			$.ajax({
				url: 'setting/1/edit',
				type: 'GET',
				dataType: 'JSON',
				success: function(data) {
					$('#nama').val(data.nama_perusahaan)
					$('#alamat').val(data.alamat)
					$('#telepon').val(data.telepon)
					$('#diskon_member').val(data.diskon_member)
					$('#tipe_nota').val(data.tipe_nota)

					d = new Date();
					$('.tampil-logo').html('<img src="public/images/'+ data.logo+'?'+d.getTime()+'" width="200">');
					$('.tampil-kartu').html('<img src="public/images/'+ data.kartu_member+'?'+d.getTime()+'" width="300">');
				}, 
				error: function(data) {
					alert('Tidak dapat menyimpan data!');
				}
			})
		}
	</script>
@endsection
