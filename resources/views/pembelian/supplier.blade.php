<div class="modal fade" id="modal-supplier" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
				<h4 class="modal-title">Pilih Supplier</h4>
			</div>

			<div class="modal-body" style="overflow: auto;">
				<table class="table table-striped table-bordered tabel-supplier">
		            <thead>
		                <th>Nama Supplier</th>
		                <th>Alamat</th>
		                <th>Telpon</th>
		                <th width="100">Aksi</th>
		            </thead>
		            <tbody>
		            	@foreach($supplier as $data)
		            		<tr>
		            			<td>{{ $data->nama }}</td>
		            			<td>{{ $data->alamat }}</td>
		            			<td>{{ $data->telpon }}</td>
		            			<td><a href="pembelian/{{ $data->id_supplier }}/tambah" class="btn btn-primary btn-xs btn-flat"><i class="fa fa-check-circle"></i> Pilih</a>
		            			</td>
		            		</tr>
		            	@endforeach
		            </tbody>
		        </table>
			</div>
		</div>
	</div>
</div>