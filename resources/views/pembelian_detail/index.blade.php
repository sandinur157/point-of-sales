@extends('layouts.app')

@section('css')
<style>
	#tampil-bayar {
		font-size: 5em; 
		text-align: center; 
		height: 100px;
	}	

	#tampil-terbilang {
		padding: 10px; 
		background-color: #f0f0f0;
	}

	@media(max-width: 768px) {
		#tampil-bayar {
			font-size: 3em;
			height: 70px;
			padding-top: 5px;
		}
	}
</style>
@endsection

@section('title')
	Transaksi Pembelian
@endsection

@section('breadcrumb')
	@parent
	<li>Pembelian</li>
	<li>Tambah</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<table>
			            <tr><td width="20">Supplier</td><td>&nbsp; : {{ $supplier->nama }}</td></tr>
			            <tr><td width="20">Alamat</td><td>&nbsp; : {{ $supplier->alamat }}</td></tr>
			            <tr><td width="20">Telpon</td><td>&nbsp; : {{ $supplier->telpon }}</td></tr>
			        </table>
				</div>

			    <div class="box-body">

			        <form class="fom-horizontal form-produk" method="post">
						@csrf {{ method_field('POST') }}
						<input type="hidden" name="idpembelian" value="{{ $idpembelian }}">

						<div class="form-group row">
							<label for="kode" class="col-md-2 control-label">Kode Produk</label>
							<div class="col-md-5">
								<div class="input-group">
									<input type="hidden" name="id_produk" id="id_produk">
									<input type="text" name="kode" id="kode" class="form-control" onkeypress="showProduct()">
									<span class="input-group-btn">
										<button onclick="showProduct()" type="button" class="btn btn-info btn-flat"><i class="fa fa-arrow-right"></i> </button>
									</span>
								</div>
							</div>
						</div>
					</form>

					<form class="form-keranjang" method="post" style="overflow: auto;">
						@csrf @method('PATCH')
						<table class="table table-striped table-bordered tabel-pembelian">
						    <thead>
						        <th width="30">No</th>
						        <th>Kode Produk</th>
						        <th>Nama Produk</th>
						        <th align="right">Harga</th>
						        <th>Jumlah</th>
						        <th>Sub Total</th>
						        <th width="100">Aksi</th>
						    </thead>
						    <tbody></tbody>
						</table> <br>
						
						<style>
							.tabel-pembelian tbody tr:last-child {
								display: none;
							}
						</style>
					</form>

					<div class="col-md-8" style="padding: 0; margin: 0;">
						<div id="tampil-bayar" class="bg-primary"></div>
						<div id="tampil-terbilang"></div>
					</div>

					<div class="col-md-4">
						<form class="fom-horizontal form-pembelian" method="post" action="{{ route('pembelian.store') }}">
							@csrf @method('POST')
							<input type="hidden" name="idpembelian" value="{{ $idpembelian }}">
							<input type="hidden" name="total" id="total">
							<input type="hidden" name="totalitem" id="totalitem">
							<input type="hidden" name="bayar" id="bayar">

							<div class="form-group row">
								<label for="totalrp" class="col-md-2 control-label">Total</label>
								<div class="col-md-8">
									<input type="text" name="totalrp" id="totalrp" class="form-control" readonly>
								</div>
							</div>

							<div class="form-group row">
								<label for="diskon" class="col-md-2 control-label">Diskon</label>
								<div class="col-md-8">
									<input type="number" name="diskon" id="diskon" class="form-control" value="0">
								</div>
							</div>

							<div class="form-group row">
								<label for="bayarrp" class="col-md-2 control-label">Bayar</label>
								<div class="col-md-8">
									<input type="text" name="bayarrp" id="bayarrp" class="form-control" readonly>
								</div>
							</div>
						</form>
					</div>
			    </div>

			    <div class="box-footer">
					<button type="submit" class="btn btn-primary btn-sm btn-flat pull-right simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
				</div>
			</div>
		</div>
	</div>
	@include('pembelian_detail.produk')
@endsection

@section('script')
	<script>
		var table;
		
		$(function() {
			$('body').addClass('sidebar-collapse')
			$('.tabel-produk').DataTable();

			table = $('.tabel-pembelian').DataTable({
				'dom' : 'Brt',
				'bSort' : false,
				'processing' : true,
				'autoWidth' : false,
				'ajax' : {
					'url' : '{{ route('pembelian_detail.data', $idpembelian) }}',
					'type' : 'GET'
				}
			}).on('draw.dt', function() {
				loadForm($('#diskon').val());
			})
		})

		// menghindari submit pada saat di enter
		$('.form-produk, .form-keranjang').on('submit', function() {
			return false;
		})

		// proses ketika kode produk atau diskon diubah
		$('#kode').change(function() {
			addItem();
		})

		$('#diskon').change(function() {
			if($(this).val() == '') $(this).val(0).select();
			loadForm($(this).val());
		})

		// menyimpan form transaksi
		$('.simpan').click(function() {
			$('.form-pembelian').submit();
		})

		function addItem() {
			$.ajax({
				url : '{{ route('pembelian_detail.store') }}',
				type : 'POST',
				data : $('.form-produk').serialize(),
				success : function(data) {
					$('#kode').val('').focus();
					table.ajax.reload(function(){
						loadForm($('#diskon').val())
					})
				}, 
				error : function() {
					alert('Tidak dapat menyimpan data!');
				}
			})
		}

		function selectItem(id, kode) {
			$('#id_produk').val(id);
			$('#kode').val(kode);
			$('#modal-produk').modal('hide');
			addItem();
		}

		function changeCount(id) {
			$.ajax({
				url  : 'pembelian_detail/'+ id,
				type : 'POST',
				data : $('.form-keranjang').serialize(),
				success : function(data) {
					table.ajax.reload(function() {
						loadForm($('#diskon').val());
					})
				}, 
				error : function() {
					alert('Tidak dapat menyimpan data!');
				}
			})
		}

		function showProduct() {
			$('#modal-produk').modal('show');
		}

		function deleteItem(id) {
			if(confirm('Apakah yakin data akan dihapus?')) {
				$.ajax({
					url : 'pembelian_detail/'+ id,
					type : 'POST',
					data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
					success :  function(data) {
						table.ajax.reload(function() {
							loadForm($('#diskon'));
						})
					},
					error : function() {
						alert('Tidak dapat menghapus data!');
					}
				})
			}
		}

		function loadForm(diskon) {
			$('#total').val($('.total').text());
			$('#totalitem').val($('.totalitem').text());
			
			if(diskon) diskon = diskon;
			else diskon = 0;

			$.ajax({
				url : 'pembelian_detail/loadform/'+ diskon + '/' + $('.total').text(),
				type : 'GET',
				dataType : 'JSON',
				success : function(data) {
					$('#totalrp').val('Rp. '+ data.totalrp);
					$('#bayarrp').val('Rp. '+ data.bayarrp);
					$('#bayar').val(data.bayar);
					$('#tampil-bayar').text('Rp. '+ data.bayarrp);
					$('#tampil-terbilang').text('Rp. '+ data.terbilang);
				},
				error : function(data) {
					// alert('Tidak dapat menampilkan data!');
				}
			})
		}
	</script>
@endsection
