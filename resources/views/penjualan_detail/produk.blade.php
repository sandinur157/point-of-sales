<div class="modal fade" id="modal-produk" tabindex="1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times; </span></button>
				<h4 class="modal-title">Cari Produk</h4>
			</div>

			<div class="modal-body" style="overflow: auto;">
				<table class="table table-striped table-bordered tabel-produk">
				    <thead>
				        <th>Kode Produk</th>
				        <th>Nama Produk</th>
				        <th>Harga Jual</th>
				        <th>Stok</th>
				        <th>Aksi</th>
				    </thead>
				
				    <tbody>
				        @foreach($produk as $data)
				        	<tr>
				        		<td>{{ $data->kode_produk }}</td>
				        		<td>{{ $data->nama_produk }}</td>
				        		<td>{{ format_uang($data->harga_jual) }}</td>
				        		<td>{{ $data->stok }}</td>
				        		<td>
				        		@if ($data->stok != 0) 
				        			<a onclick="selectItem({{ $data->id_produk }}, {{ $data->kode_produk }})" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-check-circle"></i> Pilih</a>
				        		@else
				        			<a onclick="selectItem({{ $data->id_produk }}, {{ $data->kode_produk }})" class="btn btn-default btn-sm btn-flat disabled"><i class="fa fa-check-circle"></i> Pilih</a>
				        		@endif
				        		</td>
				        	</tr>
				        @endforeach
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>