<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Nota Kecil</title>
	<style>
		* {
			font-family: 'consolas', sans-serif;
		}
		span {display: block; margin: 3px;}
		.clear {
			clear: both;
		}
		@media print {
			@page 
	        {
	            margin: 0mm;
	        }
		}
	</style>
</head>
<body style="width: 300px; margin: auto; padding: .5%;" onload="window.print()">
	
	<div style="text-align: center;">
		<h3 style="margin-bottom: 5px;">{{ strtoupper($setting->nama_perusahaan) }}</h3>
		<span>{{ $setting->alamat }}</span>
	</div>
	<br>
	<div>
		<span style="float: left;">{{ (date('Y-m-d')) }}</span>
		<span style="float: right; margin-right: .3em;">{{ Auth::user()->name }}</span>
	</div>
	<div class="clear"></div>
	<span>No : 000000{{ $penjualan->id_penjualan }}</span>

	<span>===============================</span><br>
	<table border="0" width="100%">
		@foreach($detail as $list)
		<tr>
			<td colspan="2">{{ $list->produk->nama_produk }}</td>
		</tr>
		<tr>
			<td>{{ $list->jumlah }} x {{ format_uang($list->harga_jual) }}</td> 
			<td></td> 
			<td align="right">{{ format_uang(($list->jumlah * $list->harga_jual)) }}</td>
		</tr>
		@endforeach
	</table>
	
	<span style="text-align: center;">- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</span>
	<table border="0" width="100%">
		<tr>
			<td>Total Harga:</td> 
			<td align="right">{{ format_uang($penjualan->total_harga) }}</td>
		</tr>
		<tr>
			<td>Total Item:</td> 
			<td align="right">{{ format_uang($penjualan->total_item) }}</td>
		</tr>
		<tr>
			<td>Diskon Member:</td> 
			<td align="right">{{ format_uang($penjualan->diskon) }}%</td>
		</tr>
		<tr>
			<td>Total Bayar:</td> 
			<td align="right">{{ format_uang($penjualan->bayar) }}</td>
		</tr>
		<tr>
			<td>Diterima:</td> 
			<td align="right">{{ format_uang($penjualan->diterima) }}</td>
		</tr>
		<tr>
			<td>Kembali:</td> 
			<td align="right">{{ format_uang($penjualan->diterima - $penjualan->bayar) }}</td>
		</tr>
	</table>

	<span>===============================</span>
	<span style="text-align: center;">-- TERIMA KASIH --</span>

</body>
</html>