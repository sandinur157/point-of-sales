@extends('layouts.app')

@section('title')
	Dashboard
@endsection

@section('bredcrumb')
	@parent
	<li>Dashboard</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
			    <div class="box-body">
			    	<div class="alert alert-success alert-dismissible">
			    		<i class="fa fa-check icon"></i>
			    		Data Transaksi telah disimpan.
			    	</div>
			    </div>
			    <div class="box-footer">
			    	@if ($setting->tipe_nota == 0)
			    		<a onclick="notaKecil()" class="btn btn-warning btn-flat">Cetak Ulang Nota</a>
			    	@else
			    		<a onclick="tampilNota()" class="btn btn-warning btn-flat">Cetak Ulang Nota</a>
			    	@endif

			    	<a href="{{ route('transaksi.new') }}" class="btn btn-primary btn-flat">Transaksi Baru</a>
			    </div>
			</div>
		</div>
	</div>
@endsection

@section('script')
	<script>

		function tampilNota() {
			window.open("{{ route('transaksi.pdf') }}", "Nota PDF", "height=675, width=1024, left=175, scrollbars=yes");
		}

		function notaKecil() {
        	window.open("{{ url('transaksi/cetaknota/kecil') }}", "Cetak Nota Kecil", "height=508, width=325, left=520, top=120, scrollbars=yes");
      }
	</script>
@endsection