Aplikasi Point Of Sales / Aplikasi Kasir berbasis Web
Dibuat dengan PHP Framework Laravel v5.6, Jquery, Chart.JS, DataTable, AJAX, Dll.

Fitur :
- Manajemen Produk Berdasarkan Kategori, Ketika kategori yang dipakai diproduk dipakai Maka kategori tidak bisa di hapus. (Relation Database)
- Multiple Delete, Cetak Barcode, /3 produk atau semua
- Cetak kartu member
- Manajemen Supplier
- Manajemen User
- Manajemen Pengeluaran
- Proses Pembelian berdasarkan Supplier, yang sudah diinput di tabel supplier 
- Proses Penjualan Bisa dilihat digambar
- Mendukung Barcode Reader / Scanner
- Laporan Bulanan
- Laporan Harian
- Laporan Laba & Rugi
- Barang Habis Tidak Dapat Di Klik
- Print Nota Langsung setelah Transaksi selesai.
- Tipe Nota : kecil, besar (bisa dilihat digambar)
- User (Administrator + Kasir)
- Setiap Jenis User Memiliki Hak Akses Masing Masing
- Grafik dasboard berdasarkan (penjualan, pembelian, dan pengeluaran)
- Dll

Keunggulan :
- Menggunakan laravel 5.6 yang mendukung PHP versi terbaru v7.0 - v7.3
- Relasation database benar-benar diterpakan, baik itu (RESTRICT, SET NULL, NO ACTION, CASCADE) dan dibuat melalui migration, sehingga ketika kita pasang dikomputer manapun kita tinggal lakukan migrate-migrate selesai 
- Desain pdf yang sangat cantik, DenganLaravel DomPDF & bootstrap v3.7
- Kemudahan dalam mengcustomize (nama logo, desain kartu member, desain logo) sudah tersedia di pengaturan
- Desain aplikasi yang sangat bagus, mobile friendly, responsive diukuran (HP, Tablet, Laptop) dll
- Menggunakan JQuery, DataTable Server Side Untuk shorting tabel berdasarkan apapun yang diketikan, pagination ajax, dan Chart.JS sebagai pemanis tampilan dashboard.
- Aplikasi dibuat dengan full AJAX, aplikasi sangat ringan & cepat
- Penulisan kode oleh creator dibuat dengan sangat rapi, sehingga buat programmer PHP menengah / Laravel Pemulah saya yakin bisa memahami alur aplikasi
- Desain Template menggunakan AdminLTE v2.4 yang sudah tidak asing bagi para programmer, untuk mengcustomize sendiri.