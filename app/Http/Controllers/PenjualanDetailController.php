<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use PDF;
use App\Penjualan;
use App\Produk;
use App\Member;
use App\Setting;
use App\PenjualanDetail;

class PenjualanDetailController extends Controller
{
    public function index()
    {
    	$produk = Produk::all();
    	$member = Member::all();
    	$setting = Setting::all();

    	// Cek apakah ada transaksi yang sedang berjalan
        if(session('idpenjualan')) {
            $idpenjualan = session('idpenjualan');
            return view('penjualan_detail.index', compact('produk', 'member', 'setting', 'idpenjualan'));
    	} else {
            if (auth()->user()->level == 1) return redirect()->route('transaksi.new');
            else return redirect()->route('home');
        }
    }

    public function listData($id)
    {
    	$detail = PenjualanDetail::with('produk')->where('id_penjualan', '=', $id)->get();
		
		$no         = 0;
		$data       = array();
		$total      = 0;
		$total_item = 0;

		foreach ($detail as $list) {
    		$no++;
    		$row = array();
    		$row[] = $no;
    		$row[] = $list->produk['kode_produk'];
    		$row[] = $list->produk['nama_produk'];
    		$row[] = 'Rp. '. format_uang($list->harga_jual);
    		$row[] = '
				<input type="number" class="form-control input-sm" name="jumlah'. $list->id_penjualan_detail .'" value="'. $list->jumlah .'" onChange="changeCount('.$list->id_penjualan_detail.','.$list->stok.')" id="count">
            ';
            $row[] = $list->diskon .'%';
            $row[] = 'Rp. '. format_uang($list->sub_total);
            $row[] = '
                    <a onclick="deleteItem('. $list->id_penjualan_detail .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;

            $total += $list->harga_jual * $list->jumlah;
            $total_item += $list->jumlah;
    	}
    	$data[] = [
            '<span class="total hide">'.$total.'</span> <span class="totalitem hide">'.$total_item.'</span>',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ];

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function store(Request $request)
    {
    	$produk = Produk::where('id_produk', '=', $request['id_produk'])->first();
    	$detail = new PenjualanDetail;
    	$detail->id_penjualan = $request['idpenjualan'];
    	$detail->id_produk = $request['id_produk'];
    	$detail->harga_jual = $produk->harga_jual;
    	$detail->jumlah = 1;
    	$detail->diskon = $produk->diskon;

    	// sub total disesuaikan dengan diskon
    	$detail->sub_total = $produk->harga_jual - ($produk->diskon/100 * $produk->harga_jual);
    	$detail->save();
    }

    public function update(Request $request, $id)
    {
    	$nama_input = 'jumlah'. $id;
    	$detail = PenjualanDetail::find($id);
    	$total_harga = $request[$nama_input] * $detail->harga_jual;
    	
    	$detail->jumlah = $request[$nama_input];
    	$detail->sub_total = $total_harga - ($detail->diskon/100 * $total_harga);
    	$detail->update();
    }

    public function destroy($id)
    {
    	$detail = PenjualanDetail::find($id);
    	$detail->delete();
    }

    public function newSession()
    {
    	$penjualan = new Penjualan;
    	$penjualan->id_member = null;
    	$penjualan->total_item = 0;
    	$penjualan->total_harga = 0;
    	$penjualan->diskon = 0;
    	$penjualan->bayar = 0;
    	$penjualan->diterima = 0;
    	$penjualan->id_user = \Auth::user()->id;
    	$penjualan->save();

    	session(['idpenjualan' => $penjualan->id_penjualan]);
    	return Redirect::route('transaksi.index');
    }

    public function saveData(Request $request)
    {
    	$penjualan = Penjualan::find($request['idpenjualan']);
    	$penjualan->id_member = ($request['id_member'] != 0 ? $request['id_member'] : null);
    	$penjualan->total_item = $request['totalitem'];
    	$penjualan->total_harga = $request['total'];
    	$penjualan->diskon = $request['diskon'];
    	$penjualan->bayar = $request['bayar'];
    	$penjualan->diterima = $request['diterima'];
    	$penjualan->update();

    	// Update atok data sesuai data yang dibeli
    	$detail = PenjualanDetail::where('id_penjualan', '=', $request['idpenjualan'])->get();
    	foreach ($detail as $data) {
    		$produk = Produk::where('id_produk', '=', $data->id_produk)->first();
    		$produk->stok -= $data->jumlah;
    		$produk->update();
    	}
    	return redirect(route('transaksi.cetak'));
    }

    public function loadForm($diskon = 0, $total, $diterima)
    {
    	$bayar = $total - ($diskon / 100 * $total);
    	$kembali = ($diterima != 0) ? $diterima - $bayar : 0;
    	$data = [
    		'totalrp' => format_uang($total),
    		'bayar' => $bayar,
    		'bayarrp' => format_uang($bayar),
    		'terbilang' => ucwords(terbilang($bayar)). ' Rupiah',
    		'kembalirp' => format_uang($kembali),
    		'kembaliterbilang' => ucwords(terbilang($kembali)). ' Rupiah'
    	];

    	return response()->json($data);
    }

    public function printNota()
    {
        $setting = Setting::first();

        return view('penjualan_detail.selesai', compact('setting'));
    }

    public function printNotaKecil()
    {
        $setting = Setting::first();
        $detail = PenjualanDetail::with('produk')
                ->where('id_penjualan', '=', session('idpenjualan'))
                ->get();
                
        $penjualan = Penjualan::find(session('idpenjualan'));
        $no = 0;

        return view('penjualan_detail.notakecil', compact('setting', 'detail', 'penjualan', 'no'));
    }

    public function notaPDF()
    {
    	$detail = PenjualanDetail::with('produk')
    			->where('id_penjualan', '=', session('idpenjualan'))
    			->get();
    			
    	$penjualan = Penjualan::find(session('idpenjualan'));
    	$setting = Setting::first();
    	$no = 0;

    	$pdf = PDF::loadView('penjualan_detail.notapdf', compact('detail', 'penjualan', 'setting', 'no'));
    	$pdf->setPaper(array(0,0,609,440), 'potrait');
    	return $pdf->stream();
    }
}
