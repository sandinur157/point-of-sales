<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\Setting;
use PDF;

class MemberController extends Controller
{
	public function index() 
    {
    	return view('member.index');
    }

    public function listData()
    {
    	$member = Member::orderBy('id_member', 'desc')->get();
        $no = 0;
        $data = array();

        foreach ($member as $list) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="id[]" value="'. $list->id_member .'" style="cursor: pointer;">';
            $row[] = $no;
            $row[] = '<sup class="label label-success">'. $list->kode_member .'</sup>';
            $row[] = $list->nama;
            $row[] = $list->alamat;
            $row[] = $list->telpon;
            $row[] = '
                    <a onclick="editForm('. $list->id_member .')" class="btn btn-xs btn-flat btn-info"><i class="fa fa-pencil"></i> Edit</a>
                    <a onclick="deleteData('. $list->id_member .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function store(Request $request)
    {
    	$jml = Member::where('kode_member', '=', $request['kode'])->count();
        
        if($jml < 1) {
        
	        $member = new Member;
	        $member->kode_member = $request['kode'];
	        $member->nama = $request['nama'];
	        $member->alamat = $request['alamat'];
	        $member->telpon = $request['telpon'];
	        $member->save();
	        echo json_encode(array('msg' => 'success'));
	    } else {
	    	echo json_encode(array('msg' => 'error'));
	    }
    }

    public function edit($id)
    {
        $member = Member::find($id);
        echo json_encode($member);
    }

    public function update(Request $request, $id) 
    {
    	$member = Member::find($id);
        $member->kode_member = $request['kode'];
        $member->nama = $request['nama'];
        $member->alamat = $request['alamat'];
        $member->telpon = $request['telpon'];
        $member->update();
        echo json_encode(array('msg' => 'success'));
    }

    public function destroy($id)
    {
    	$member = Member::find($id);
    	$member->delete();
    }

    public function printCard(Request $request) 
    {
    	$datamember = array();
        $card = Setting::select('kartu_member')->first();
    	foreach ($request['id'] as $id) {
    		$member = Member::find($id);
    		$datamember[] = $member;
    	}

    	$pdf = PDF::loadView('member.card', compact('datamember', 'card'));
    	$pdf->setPaper(array(0, 0, 566.93, 850.39), 'potrait');
    	return $pdf->stream('member');
    }

}
