<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\User;
use App\Penjualan;
use App\Produk;
use App\Member;
use App\PenjualanDetail;

class PenjualanController extends Controller
{
	public function index()
    {
        return view('penjualan.index');
    }

    public function listData() 
    {
        $penjualan = Penjualan::with(['user', 'member'])->orderBy('id_penjualan', 'desc')->get();

        $no = 0;
        $data = array();
        
        foreach ($penjualan as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tanggal_indonesia(substr($list->created_at, 0, 10));
            $row[] = $list->member['kode_member'];
            $row[] = $list->total_item;
            $row[] = 'Rp. '. format_uang($list->total_harga);
            $row[] = $list->diskon. '%';
            $row[] = 'Rp. '. format_uang($list->bayar);
            $row[] = $list->user['name'];
            $row[] = '
            		<a onclick="showDetail('. $list->id_penjualan .')" class="btn btn-xs btn-flat btn-primary"><i class="fa fa-eye"></i> Lihat</a>
                    <a onclick="deleteData('. $list->id_penjualan .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function show($id)
    {
    	$detail = PenjualanDetail::with('produk')->where('id_penjualan', '=', $id)->get();

        $no = 0;
        $data = array();
        
        foreach ($detail as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->produk['kode_produk'];
            $row[] = $list->produk['nama_produk'];
            $row[] = 'Rp. '. format_uang($list->harga_jual);
            $row[] = $list->jumlah;
            $row[] = 'Rp. '. format_uang($list->sub_total);
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function destroy($id)
    {
    	$penjualan = Penjualan::find($id);
    	$penjualan->delete();

    	$detail = PenjualanDetail::where('id_penjualan', '=', $id)->get();
    	foreach ($detail as $data) {
    		$produk = Produk::where('id_produk', '=',  $data->id_produk)->first();
    		$produk->stok += $data->jumlah;
    		$produk->update();
    		$produk->delete();
    	}
    }

}
