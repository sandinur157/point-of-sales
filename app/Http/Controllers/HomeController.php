<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;

use App\Setting;
use App\Kategori;
use App\Produk;
use App\Supplier;
use App\Member;
use App\Penjualan;
use App\Pembelian;
use App\Pengeluaran;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
	}

    public function index()
    {
    	$setting = Setting::first();

        $awal = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
    	$akhir = date('Y-m-d');

    	$tanggal = $awal;
    	$data_tanggal = array();
    	$data_pendapatan = array();

    	while (strtotime($tanggal) <= strtotime($akhir)) {
    		$data_tanggal[] = (int) substr($tanggal, 8, 2);
    		
    		$total_penjualan = Penjualan::where('created_at', 'LIKE', "%$tanggal%")->sum('bayar');
            $total_pembelian = Pembelian::where('created_at', 'LIKE', "%$tanggal%")->sum('bayar');
            $total_pengeluaran = Pengeluaran::where('created_at', 'LIKE', "%$tanggal%")->sum('nominal');
            
            $pendapatan = $total_penjualan - $total_pembelian - $total_pengeluaran;
    		$data_pendapatan[] += (int) $pendapatan;
    		
    		$tanggal = date('Y-m-d', strtotime("+1 day", strtotime($tanggal)));
    	}

    	$kategori = Kategori::count();
    	$produk = Produk::count();
    	$supplier = Supplier::count();
    	$member = Member::count();

    	if(Auth::user()->level == 1)
    		return view('home.admin', compact('kategori', 'produk', 'supplier', 'member', 'awal', 'akhir', 'data_pendapatan', 'data_tanggal'));
    	else return view('home.kasir', compact('setting'));
    }
}
