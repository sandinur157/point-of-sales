<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pembelian;
use App\Supplier;
use App\PembelianDetail;
use App\Produk;

class PembelianController extends Controller
{
    public function index()
    {
    	$supplier = Supplier::all();
        return view('pembelian.index', compact('supplier'));
    }

    public function listData() 
    {
        $pembelian = Pembelian::with('supplier')->orderBy('id_pembelian', 'desc')->get();

        $no = 0;
        $data = array();

        foreach ($pembelian as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = tanggal_indonesia(substr($list->created_at, 0, 10));
            $row[] = $list->supplier['nama'];
            $row[] = $list->total_item;
            $row[] = 'Rp. '. format_uang($list->total_harga);
            $row[] = $list->diskon. '%';
            $row[] = 'Rp. '. format_uang($list->bayar);
            $row[] = '
                    <a onclick="showDetail('. $list->id_pembelian .')" class="btn btn-xs btn-flat btn-info"><i class="fa fa-eye"></i> Lihat</a>
                    <a onclick="deleteData('. $list->id_pembelian .')" class="btn btn-xs btn-flat btn-danger"><i class="fa fa-trash"></i> Hapus</a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function show($id) 
    {
    	$detail = PembelianDetail::with('produk')->where('id_pembelian', '=', $id)->get();
        $no = 0;
        $data = array();

        foreach ($detail as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->produk['kode_produk'];
            $row[] = $list->produk['nama_produk'];
            $row[] = 'Rp. '. format_uang($list->harga_beli);
            $row[] = $list->jumlah;
            $row[] = 'Rp. '. format_uang($list->harga_beli * $list->jumlah);
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function create($id)
    {
    	$pembelian = new Pembelian;
		$pembelian->id_supplier = $id;
		$pembelian->total_item  = 0;
		$pembelian->total_harga = 0;
		$pembelian->diskon      = 0;
		$pembelian->bayar       = 0;
    	$pembelian->save();

    	session(['idpembelian' => $pembelian->id_pembelian]);
    	session(['idsupplier' => $id]);

    	return Redirect::route('pembelian_detail.index');
    }

    public function store(Request $request)
    {
        $pembelian = Pembelian::find($request['idpembelian']);
        $pembelian->total_item = $request['totalitem'];
        $pembelian->total_harga = $request['total'];
        $pembelian->diskon = $request['diskon'];
        $pembelian->bayar = $request['bayar'];
        $pembelian->update();

        $detail = PembelianDetail::where('id_pembelian', '=', $request['idpembelian'])->get();
        foreach ($detail as $data) {
        	$produk = Produk::where('id_produk', '=', $data->id_produk)->first();
        	$produk->stok += $data->jumlah;
        	$produk->update();
        }

        return Redirect::route('pembelian.index');
    }

    public function destroy($id)
    {
    	$pembelian = Pembelian::find($id);
    	$pembelian->delete();

    	$detail = PembelianDetail::where('id_pembelian', '=', $id)->get();
    	foreach ($detail as $data) {
    		$produk = Produk::where('kode_produk', '=', $data->kode_produk)->first();
    		$produk->stok -= $data->jumlah;
    		$produk->update();
    		$produk->delete();
    	}
    }
}
