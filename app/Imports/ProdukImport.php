<?php

namespace App\Imports;

use App\Produk;
use Maatwebsite\Excel\Concerns\ToModel;

class ProdukImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Produk([
            'id_kategori' => '',
            'kode_produk' => $row[1],
            'id_kategori' => $row[2],
            'nama_produk' => $row[3],
            'merk'        => $row[4],
            'harga_beli'  => $row[5],
            'diskon'      => 0,
            'stok'        => $row[7],
            'harga_jual'  => $row[8],
        ]);
    }
}
