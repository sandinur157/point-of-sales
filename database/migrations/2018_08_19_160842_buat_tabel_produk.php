<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelProduk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produk', function(Blueprint $table) {
            $table->increments('id_produk');
            $table->bigInteger('kode_produk');
            $table->integer('id_kategori')->unsigned();
            $table->string('nama_produk', 100);
            $table->string('merk', 50);
            $table->bigInteger('harga_beli');
            $table->integer('diskon');
            $table->bigInteger('harga_jual');
            $table->integer('stok');
            $table->timestamps();
        });

        Schema::table('produk', function(Blueprint $table) {
            $table->foreign('id_kategori')->references('id_kategori')->on('kategori')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
    }
}
