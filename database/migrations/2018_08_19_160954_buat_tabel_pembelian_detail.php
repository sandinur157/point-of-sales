<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPembelianDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelian_detail', function(Blueprint $table) {
            $table->increments('id_pembelian_detail');
            $table->integer('id_pembelian')->unsigned();
            $table->integer('id_produk')->unsigned();
            $table->bigInteger('harga_beli');
            $table->integer('jumlah');
            $table->bigInteger('sub_total');
            $table->timestamps();
        });

        Schema::table('pembelian_detail', function(Blueprint $table) {
            $table->foreign('id_pembelian')->references('id_pembelian')->on('pembelian')->onDelete('cascade')->onUpdate('cascade');
        });
        
        Schema::table('pembelian_detail', function(Blueprint $table) {
            $table->foreign('id_produk')->references('id_produk')->on('produk')->onDelete('restrict')->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelian_detail');
    }
}
