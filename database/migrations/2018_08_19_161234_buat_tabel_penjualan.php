<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan', function(Blueprint $table) {
            $table->increments('id_penjualan');
            $table->integer('id_member')->nullable()->unsigned();
            $table->integer('total_item');
            $table->bigInteger('total_harga');
            $table->integer('diskon');
            $table->bigInteger('bayar');
            $table->bigInteger('diterima');
            $table->integer('id_user')->unsigned();
            $table->timestamps();
        });

        Schema::table('penjualan', function(Blueprint $table) {
            $table->foreign('id_member')->references('id_member')->on('member')->onDelete('set null')->onUpdate('set null');
        });

        Schema::table('penjualan', function(Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan');
    }
}
