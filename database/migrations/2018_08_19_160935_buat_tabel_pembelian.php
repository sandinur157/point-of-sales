<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembelian', function(Blueprint $table) {
            $table->increments('id_pembelian');
            $table->integer('id_supplier')->unsigned();
            $table->integer('total_item');
            $table->bigInteger('total_harga');
            $table->integer('diskon');
            $table->bigInteger('bayar');
            $table->timestamps();
        });

        Schema::table('pembelian', function(Blueprint $table) {
            $table->foreign('id_supplier')->references('id_supplier')->on('supplier')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembelian');
    }
}
