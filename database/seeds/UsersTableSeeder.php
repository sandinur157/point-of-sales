<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
        	[
        		'name' => 'Administrator',
        		'email' => 'admin@gmail.com',
        		'password' => bcrypt('123'),
        		'foto' => 'admin.jpg',
        		'level' => 1
        	],
        	[
        		'name' => 'user',
        		'email' => 'user@gmail.com',
        		'password' => bcrypt('123'),
        		'foto' => 'user.jpg',
        		'level' => 2
        	]
        ));
    }
}
